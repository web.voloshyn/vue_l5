import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

Vue.config.productionTip = false

new Vue({
  router, /* интеграция ротера для получения доступа с любого компонента */
  store,
  render: h => h(App)
}).$mount('#app')
