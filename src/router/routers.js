
import RHome from '@/views/router/Home.vue'
import RSingleNews from '@/views/router/SingleNews.vue'
import RNewsWithSingle from '@/views/router/NewsWithSingle.vue'


export default [

  {
    path: '/r-home',
    name: 'r-home',
    // redirect: 'path', {name: ''}, (to) => {должно вернуть строку или обьект пути}
    alias: '/r-home-page',
    /*Для использования именованного представления, нужно использовать свойство "components" с "s" в конце*/
    components: {
      default: RHome,
      addNav: () => import('@/views/router/Nav.vue')
    }
  },
  {
    path: '/r-about',
    name: 'r-about',
    components: {
      default: () => import('@/views/router/About.vue'),
      addNav: () => import('@/views/router/Nav.vue')
    }
  },
  {
    path: '/r-news',
    name: 'r-news',
    components: {
      default: () => import('@/views/router/News.vue'),
      addNav: () => import('@/views/router/Nav.vue')
    }
  },
  {
    path: '/r-news/:id',
    name: 'r-single-news',
    component: RSingleNews,
    components: {
      default: RSingleNews,
      addNav: () => import('@/views/router/Nav.vue')
    }
  },
  {
    path: '/r-news-with',
    name: 'r-news-with',
    components: {
      default: () => import('@/views/router/NewsWithSingle.vue'),
      addNav: () => import('@/views/router/Nav.vue')
    },
    children: [
      /* Вложенные маршруты - в родительском компоненте должен быть router-view
      для отображения компонента дочернего роута(компонента) */
      {
        name: 'news-with-single',
        path: ':id',
        component: RSingleNews
      }
    ]
  },
  
]