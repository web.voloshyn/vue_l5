/*

    Задание 1.

      - Создать компоненты, HomePage, About, Contacts, List, Item
        (Компоненты просто пустые и выводят просто заголовок компонента)
      - Роуты:
              /             -> HomePage
              /list         -> List
              /list/:itemid -> Item
              /about        -> About
              /contacts     -> Contacts

      - Создать компонент меню
      (Компоненты просто пустые и выводят просто заголовок компонента)


    Задание 2.

    - Сгенерировать список артистов исходя из любого из JSON выше.
      http://www.json-generator.com/api/json/get/cuKKbBWWXm?indent=2
      http://www.json-generator.com/api/json/get/bGyYXYiCOG?indent=2
      http://www.json-generator.com/api/json/get/bTUcoMILQO?indent=2


    - после перехода на артиста:
        - показывать список его альбомов и количество композиций в нем.
        - показывать общее время всех треков

    - переход в альбом и посмотреть все треки
    - возможность выставить рейтинг от 1 до 5 звезд каждому треку

*/
